'use strict';
window.addEventListener('DOMContentLoaded', enable_disable_Button);
/**
 * enable/disable button
 */
function enable_disable_Button() {
    let booleanField = {
        proj_id:validateElement(idElem, regexProjId),
        name:validateElement(nameElem, regexName),
        title:validateElement(titleElem, regexTitle),
        cat:validateElement(catElem, regexCat),
        hours:validateElement(hourElem, regexHours),
        rate:validateElement(rateElem, regexRate),
        status:validateElement(statusElem, regexStatus),
        descr: validateElement(descrElem, regexDescr)
    }
    for (const v of Object.values(booleanField)) {
        if (v == false) {
            document.getElementById("add").disabled = true;
            break;
        }
        else {
            document.getElementById("add").disabled = false;
        }
    }
}

/**
 * A function that shows an image if its valid or not
 * @param {Object} elem 
 * @param {Object} pattern 
 */
function setElementFeedBack(elem, pattern) {
    if (document.getElementById(elem.id).nextSibling != undefined){
        document.getElementById(elem.id).nextSibling.remove();
        if (pattern.test(elem.value)) {
            let spanImg = document.createElement("span");
            spanImg.setAttribute("class", "req");
            let img = document.createElement("img");
            img.setAttribute("src", "../images/check-mark.png");
            spanImg.appendChild(img);
            document.getElementById(elem.id).before(spanImg);
            elem.parentElement.appendChild(spanImg);
        }
        else {
            let spanImg = document.createElement("span");
            spanImg.setAttribute("class", "req");
            let img = document.createElement("img");
            img.setAttribute("src", "../images/invalid.png");
            let pTag = document.createElement("p");
            pTag.textContent = "Wrong format for " + elem.id;
            spanImg.appendChild(img);
            spanImg.appendChild(pTag);
            document.getElementById(elem.id).before(spanImg);
            elem.parentElement.append(spanImg);
        }

    }
    else{
        if (pattern.test(elem.value)) {
            let spanImg = document.createElement("span");
            spanImg.setAttribute("class", "req");
            let img = document.createElement("img");
            img.setAttribute("src", "../images/check-mark.png");
            spanImg.appendChild(img);
            document.getElementById(elem.id).before(spanImg);
            elem.parentElement.append(spanImg);
        }
        else {
            let spanImg = document.createElement("span");
            spanImg.setAttribute("class", "req");
            let img = document.createElement("img");
            img.setAttribute("src", "../images/invalid.png");
            let pTag = document.createElement("p");
            pTag.textContent = "Wrong format for " + elem.id;
            spanImg.appendChild(img);
            spanImg.appendChild(pTag);
            document.getElementById(elem.id).before(spanImg);
            spanImg.parentElement.append(spanImg);
        }
    }
}

/**
 * a function to compare the input of elem and a regex pattern
 * @param {object} elem 
 * @param {object} pattern 
 * @returns boolean
 */
function validateElement(elem, pattern) {
    if (pattern.test(elem.value)) {
        return true;
    }
    else {
        return false;
    }

}

