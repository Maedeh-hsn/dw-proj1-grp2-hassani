'use strict';

/**
 * the function to add a row each time, when we use add buttons - it also set an id for the delete buttons
 * and make this button active to delete the specific rows.
 */
let edit; let counter_edit = 0;
document.querySelector("#add").addEventListener('click', updateProjectTable);

function updateProjectTable() {

    document.getElementById("table").style.display="block";
            
    let table = document.getElementById("table");
    let row = table.insertRow(-1);
    let projectId = row.insertCell(0);
    let ownerName = row.insertCell(1);
    let title = row.insertCell(2);
    let category = row.insertCell(3);
    let status = row.insertCell(4);
    let hours = row.insertCell(5);
    let rate = row.insertCell(6);
    let description = row.insertCell(7);
    let edit = row.insertCell(8);
    let  del = row.insertCell(9);

    projectId.textContent =  document.querySelector("#project-id").value;
    ownerName.textContent = document.querySelector("#owner-name").value;
    title.textContent = document.querySelector("#title").value;
    category.textContent = document.querySelector("#category").value;
    status.textContent = document.querySelector("#status").value;
    hours.textContent = document.querySelector("#hours").value;
    rate.textContent = document.querySelector("#rate").value;
    description.textContent = document.querySelector("#description").value;

    let edit_btn = document.createElement('div');
    let edit_img = document.createElement("img");
    edit_img.setAttribute("src", "../images/edit.png");
    edit.appendChild(edit_btn);
    edit_btn.parentElement.append(edit_img);

    edit_img.addEventListener('click', function(event) {
      let editCol =event.target.parentElement; 
      let editRow = editCol.parentElement;
      let saveDiv = document.createElement("div");
      let saveImg = document.createElement("img");
      let newValArr = [];
      saveImg.setAttribute("src", "../images/save-black.png");
      saveDiv.appendChild(saveImg);
      editCol.replaceWith(saveDiv);

      for (let i = 0 ; i<editRow.children.length - 2;i++) {
        editRow.children[i].innerHTML = `<input type ='text' id = 'intput${[i]}' value= '${editRow.children[i].textContent}'>`;
      }
      for (let x = 0; x < editRow.children.length -2; x++) {
        newValArr[x] = editRow.children[x].firstChild.value;
      }


      saveImg.addEventListener('click', () => {
        for (let j = 0 ; j < editRow.children.length -2;j++) {
          editRow.children[j].textContent = newValArr[j];

        }
      });
    });

    /*edit_btn.setAttribute("id", counter_edit);
    edit_img.addEventListener('click', function(event){
      let confrm = confirm(`Edit ??????????????? `);
        if (confrm == true)
        event.target(editTableRow());
<<<<<<< HEAD
    });*/

    let delete_btn = document.createElement('span');
    let del_img = document.createElement("img");
    del_img.setAttribute("src", "../images/delete-black.png");
    del.appendChild(delete_btn);
    delete_btn.parentElement.append(del_img); 

    delete_btn.setAttribute("id", "del" + counter_delete);
    del.appendChild(delete_btn);
    del_img.addEventListener('click', function(event){
        //let x = document.getElementById("del"+counter_delete).parentElement.parentElement.querySelector('td').textContent;
        //let x = document.getElementsByTagName('div')[counter_delete].parentElement.parentElement.querySelector('td').textContent;
        let confrm = confirm(`Are you sure to delete the project `); /*${x}*/
        if (confrm == true)
        event.target.parentElement.parentElement.remove();
    });
    
    counter_delete++;
    counter_edit++;

    project_object = {projectId: projectId.textContent, 
                          ownerName: ownerName.textContent,
                          title: title.textContent, 
                          category: category.textContent,
                          status: description.textContent,
                          hours: hours.textContent,  rate: rate.textContent,  
                          rate: rate.textContent,
                          description: description.textContent
                            };
    project_array.push(project_object); 
    

}


/**
 * clear the local storage
 */
document.getElementById("clear-loc").addEventListener('click', function(){
    if (localStorage.length == 0){
      alert(`The local storage is empty!`)
    }else{
    let confrm = confirm(`Are you sure you want to delete Local Storage?`);
        if (confrm == true)
        localStorage.clear();
    }
 })

 /**
  * store an array of project in local storage
  */
 document.getElementById("write-loc").addEventListener('click', function(){ 
    let confrm = confirm(`Do you want to save the data in Local Storage?`);
    if (confrm == true)
     localStorage.setItem("project_array", JSON.stringify(project_array));
 });
  

/**
 * An addEventListener for the append local button
 */
document.getElementById("append-loc").addEventListener('click', function() { 
if (localStorage.length == null) {
  let confirm = confirm(`There is nothing saved in the storage. Do you want to override?`);
  if (confirm == true) {
    localStorage.setItem("project_array", JSON.stringify(project_array));
  }
}
else {
    let oldArray = JSON.parse(localStorage.getItem("project_array") || "[]");
    for (let i = 0; i <project_array.length;i++) {
      oldArray.push(project_array[i]);
    }
    localStorage.setItem("project_array", JSON.stringify(oldArray));
}
});



 /**
  * addevnetlistener for the load button to retrieve the dat afro m the storage
  */
  document.getElementById("load-loc").addEventListener('click', function(){ 
    project_array = JSON.parse(localStorage.getItem("project_array") || "[]");
    let table = document.getElementById("table");
    let countLoad = document.getElementById("countLoadOrSearch");
    for (let i = 0 ; i <project_array.length;i++) {
      let newTr = document.createElement("tr");
      for (let v in project_array[i]) {
        let newTd = document.createElement("td");
        newTd.textContent = `${project_array[i][v]}`;
        newTr.appendChild(newTd);
      }
      let editDiv = document.createElement("div");
      let editTd = document.createElement("td");
      let edit_img = document.createElement("img");
      edit_img.setAttribute("src", "../images/edit.png");
      editDiv.appendChild(edit_img);
      editTd.appendChild(editDiv);
      newTr.appendChild(editTd);
      edit_img.addEventListener('click', function(event) {
        let editCol =event.target.parentElement; 
        let editRow = editCol.parentElement;
        let saveDiv = document.createElement("div");
        let saveImg = document.createElement("img");
        let newValArr = [];
        saveImg.setAttribute("src", "../images/save-black.png");
        saveDiv.appendChild(saveImg);
        editCol.replaceWith(saveDiv);
  
        for (let i = 0 ; i<editRow.children.length - 2;i++) {
          newValArr[i]=editRow.children[i].innerHTML = `<input type ='text' value= '${editRow.children[i].textContent}'>`;
        }
  
        saveImg.addEventListener('click', () => {
          for (let j = 0 ; j < editRow.children.length -2;j++) {
            editRow.children[j].textContent = newValArr[j].value;
          }
        });
      });

      let deleteTd = document.createElement("td");
      let del_img = document.createElement("img");
      del_img.setAttribute("src", "../images/delete-black.png");
      deleteTd.appendChild(del_img);
      newTr.appendChild(deleteTd);
      del_img.addEventListener('click', function(event){  
        let confrm = confirm(`Are you sure to delete the project `);
        if (confrm == true)
        event.target.parentElement.parentElement.remove();
      });

      table.appendChild(newTr);
    }
    if(countLoad.children[0] != undefined) {
      countLoad.children[0].remove();
      let pTag = document.createElement("p");
      pTag.textContent= "There are " + project_array.length + " projects in local storage...";
      countLoad.appendChild(pTag);
    }
    else {
      let pTag = document.createElement("p");
      pTag.textContent= "There are " + project_array.length + " projects in local storage...";
      countLoad.appendChild(pTag);
    }
});

/**
 * addEventListener for the search
 */
document.getElementById('query').addEventListener('keyup', filterProjects);
function filterProjects() {
  let input, filter, table, tr, td, countLoad;
  input = document.getElementById("query");
  filter = input.value.toUpperCase();
  table = document.getElementById("table");
  tr = table.getElementsByTagName("tr");
  countLoad = document.getElementById("countLoadOrSearch");
  let countTr = 0;

  for (let i = 1; i < tr.length; i++) {
    let tds = tr[i].getElementsByTagName("td");
    let flag = false;
    for(var j = 0; j < tds.length; j++){
      let td = tds[j];
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        flag = true;
      } 
    }
    if(flag){
      tr[i].style.display = "";
      countTr++;
    }
    else {
      tr[i].style.display = "none";
    }
  }
  if (countLoad.children[0] != undefined) {
    countLoad.children[0].remove();
    let pTag = document.createElement("p");
    pTag.textContent= "There is/are " + countTr + " projects for your query...";
    countLoad.appendChild(pTag);
  }
  else {
    let pTag = document.createElement("p");
    pTag.textContent= "There is/are " + countTr + " projects for your query...";
    countLoad.appendChild(pTag);
  }
}



